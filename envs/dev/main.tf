resource "azurerm_resource_group" "main" {
  name     = "terraform-test"
  location = "West Europe"
  tags = {"foo" = "barf" }

  lifecycle {
    prevent_destroy = true
  }
}

resource "random_string" "password_1" {
    length = 25
}



output "password_1" {
  value = "${random_string.password_1.result}"
}

